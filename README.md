# About this Project

These are the Celeste guides from me, 0815Ryan.

I invest a lot of time in all of these.

They are not complete.

Likely every single one of them will change at some point.

Videos can be added any day, even by people other than me.

All guides come with gifs. These gifs are embedded in the guides - that's why they take forever to load.

The gifs are in a seperate folder as well, together with the visual cue images.

You will find them if you find the guides.

Additionally the videos are also uploaded. Some gifs are slow. Some are choppy. And you can never properly navigate a gif. So this gives you all options you would want.

You can download the guide.

You can download the videos.

You can download the gifs.

This is completely free to use.

If you want to clone yourself this repository use git (gitforwindows on windows).
It is a command line but I swear it is not bad at all with one of many guides online.

I recommend MarkdownMonster for local viewing of the .md files. I just like the software and it is what I use for writing these.

# Appreciation

Most of what you see is written by me personally. If something is not purely written by me I will likely have altered it heavily because I am picky. But I will always have support from many people in the community. Too many to list at once. Some support with motivation. Some with written parts. Some record strats. Some do not even know they are helping by just being nice. Some found the strats I am talking about 5 years ago and I do not even know.
  
Please just appreciate everybody in the community. I could not write this guide without this community teaching me the strats or the skills for discovering some of them myself.